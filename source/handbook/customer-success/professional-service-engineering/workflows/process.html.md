---
layout: markdown_page
title: Process
---

# Professional Service Engineering Workflows

## Project Process
1. **Manager, Professional Services**: Once an SOW has been approved and moved for **Closed Won**, assign a Professional Service Engineer.
1. **Manager, Professional Services**: Send [welcome e-mail](/handbook/customer-success/professional-service-engineering/workflows/project_execution/welcome-email.html)
1. **Professional Service Engineer**: Begins project with processes defined here.
  - [Kick-off](/handbook/customer-success/professional-service-engineering/workflows/project_execution/kick-off.html)
  - Intake for [AWS](/handbook/customer-success/professional-service-engineering/workflows/intake/aws.html) or [on-prem](/handbook/customer-success/professional-service-engineering/workflows/intake/on-prem.html)
  - [On-going project calls (external)](/handbook/customer-success/professional-service-engineering/workflows/project_execution/calls.html)
  - [On-going internal project updates](/handbook/customer-success/professional-service-engineering/workflows/internal/15minute-standup.html)
  - [Project Summary](/handbook/customer-success/professional-service-engineering/workflows/project_execution/project-summary.html)
1. **Professional Service Engineer**: Starts [financal wrap-up](/handbook/customer-success/professional-service-engineering/workflows/internal/financial-wrapup.html) process.
1. **Manager, Professional Services**: Schedule [blameless post-mordem](/handbook/customer-success/professional-service-engineering/workflows/internal/root-cause-analysis.html)

## Quote to Sign-off Process
This is an overview of the entire process for a services engagement from discovery of the customer's needs to the sign-off from the customer.

See [Professional Services Business Operations](/handbook/customer-success/professional-service-engineering/workflows/internal/biz-ops.html)
